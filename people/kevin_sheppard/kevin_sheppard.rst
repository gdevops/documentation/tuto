.. index::
   pair: Kevin ; Sheppard
   ! Kevin Sheppard

.. _kevin_sheppard:

======================================
**Kevin Sheppard** (sphinx material)
======================================


.. seealso::

   - https://github.com/bashtage
   - https://github.com/bashtage/sphinx-material/
   - https://github.com/bashtage/sphinx-material/blob/master/docs/conf.py
   - https://github.com/bashtage/sphinx-material/graphs/contributors



.. figure:: kevin_sheppard.png
   :align: center

   https://github.com/bashtage


- <kevin.k.sheppard@gmail.com

Bio
=====

I work with Project Jupyter and other open communities to build tools
and solve problems for scientific research and education.

