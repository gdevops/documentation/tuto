
.. index::
   pair: Documentation ; mdbook
   ! mdbook

.. _mdbook:

======================================================================================
**mdbook** (Create book from markdown files. Like Gitbook but implemented in Rust)
======================================================================================

- https://github.com/rust-lang/mdBook
- https://rust-lang.github.io/mdBook/index.html


Description
=============

Introduction

**mdBook** is a command line tool to create books with Markdown.

It is ideal for creating product or API documentation, tutorials, course
materials or anything that requires a clean, easily navigable and
customizable presentation.

- `Lightweight Markdown <https://rust-lang.github.io/mdBook/format/markdown.html>`_
  syntax helps you focus more on your content
- Integrated search support
- Color syntax highlighting for code blocks for many different languages
- Theme files allow customizing the formatting of the output
- Preprocessors can provide extensions for custom syntax and modifying content
- Backends can render the output to multiple formats
- Written in Rust for speed, safety, and simplicity
- Automated testing of Rust code samples

This guide is an example of what mdBook produces.

mdBook is used by the Rust programming language project, and The Rust
Programming Language book is another fine example of mdBook in action.

