.. index::
   pair: Sphinx ; People


.. https://post.lurk.org/@kajou

.. _sphinx_people:
.. _doc_people:

======================================
People
======================================


.. toctree::
   :maxdepth: 3

   chris_holdgraph/chris_holdgraph
   chris_sewell/chris_sewell
   eric_holscher/eric_holscher
   juan_luis/juan_luis
   georg_brandl/georg_brandl
   kevin_sheppard/kevin_sheppard
   martin_donath/martin_donath
   manual_kaufman/manual_kaufman
   paul_everitt/paul_everitt
   pradyun_gedam/praydyun_gedam
   takeshi_komiya/takeshi_komiya
