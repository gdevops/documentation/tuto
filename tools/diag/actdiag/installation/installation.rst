

.. index::
   pair: actdiag ; installation

.. _actdiag_install:

=======================================
actdiag installation
=======================================

.. seealso::

   - http://blockdiag.com/en/actdiag/introduction.html#setuptools

::

    pip install actdiag

::

	Collecting actdiag
	Downloading https://files.pythonhosted.org/packages/39/13/4b7b1d738cea26fbe80d1a5546628115d10b0f02880be2b724cc8004f5db/actdiag-0.5.4-py2.py3-none-any.whl (2.6MB)
	|████████████████████████████████| 2.6MB 1.2MB/s
	Requirement already satisfied: blockdiag>=1.5.0 in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from actdiag) (1.5.4)
	Requirement already satisfied: setuptools in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from actdiag) (40.8.0)
	Requirement already satisfied: webcolors in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from blockdiag>=1.5.0->actdiag) (1.8.1)
	Requirement already satisfied: Pillow in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from blockdiag>=1.5.0->actdiag) (6.0.0)
	Requirement already satisfied: funcparserlib in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from blockdiag>=1.5.0->actdiag) (0.3.6)
	Installing collected packages: actdiag
	Successfully installed actdiag-0.5.4
