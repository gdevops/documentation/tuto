
.. index::
   pair: Documentation ; Hugo
   ! Hugo

.. _hugo:

===========================================================
Hugo (The world’s fastest framework for building websites)
===========================================================

.. seealso::

   - https://github.com/gohugoio/hugo
   - https://squidfunk.github.io/mkdocs-material/
   - https://gohugo.io/getting-started/quick-start/


.. figure:: hugo-logo-wide.svg
   :align: center
   :width: 300




README.md
===========

.. include:: README.rst


Versions
==========

.. toctree::
   :maxdepth: 3

   versions/versions
