


.. index::
   pair: readthedocs; Tuto doc


.. _tutodoc_on_readthedocs:

===================================================
Example 1 : tuto_documentation on readthedocs
===================================================


.. seealso::

   - https://gitlab.com/help/user/project/integrations/webhooks





Dashboard
==========


.. figure:: dashboard_readthedocs.png
   :align: center


URL
    https://gitlab.com/gdevops/tuto_documentation.git



Advanced dashboard
====================

.. figure:: produce_pdf.png
   :align: center

   PDF production


.. _readthedocs_gitlab:

Gitlab/readthedocs integration
================================

.. seealso::

   - https://docs.readthedocs.io/en/latest/webhooks.html


.. figure:: dashboard_readthedocs.png
   :align: center

   Readthedocs integration


.. figure:: lien_gitlab_readthedocs.png
   :align: center
