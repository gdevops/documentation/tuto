.. index::
   ! Conversion from reStructuredText to MyST

.. _rst_to_myst:

=====================================================================
**MySTyc : Online conversion from reStructuredText to MyST**
=====================================================================

- https://astrojuanlu.github.io/mystyc/
- https://github.com/astrojuanlu/mystyc
- https://www.sphinx-doc.org/en/master/usage/markdown.html
- https://myst-parser.readthedocs.io/en/latest/

Description
============

"MyST is a rich and extensible flavor of Markdown meant for technical
documentation and publishing".

It combines the familiarity of Markdown with the power and extensibility
of reStructuredText, and you can use it in your Sphinx documentation.

`Learn more! <https://myst-parser.readthedocs.io/en/latest/>`_


.. figure:: images/mystc.png
   :align: center

   https://github.com/astrojuanlu/mystyc
