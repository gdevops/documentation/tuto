.. index::
   ! Martin Donath

.. _martin_donath:

======================================
**Martin Donath** (mkdocs material)
======================================

.. seealso::

   - https://x.com/squidfunk
   - https://squidfunk.github.io/mkdocs-material/
   - https://squidfunk.github.io/mkdocs-material/getting-started/

Bio
======

Creator of Material for MkDocs · Tweeting about TypeScript, RxJS & CSS ·
Loves automata and graph theory · Working on @stylezenhq during office hours
