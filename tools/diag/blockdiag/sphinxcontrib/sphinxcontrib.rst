.. index::
   pair: sphinxcontrib ; blockdiag
   pair: sphinxcontrib ; diagram

.. _sphinxcontrib_blockdiag_bis:

=======================
sphinxcontrib-blockdiag
=======================

..  seealso::

    - https://bitbucket.org/birkenfeld/sphinx-contrib/src/e60f176286fe/blockdiag/setup.py?fileviewer=file-view-default
