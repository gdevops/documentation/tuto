
.. index::
   pair: Mariatta ; Sphinx

.. _intro_doc_sphinx_mariatta:

==============================================================
Intro to Documentation with Sphinx and reStructuredText’s
==============================================================

- https://sphinx-intro-tutorial.readthedocs.io/en/latest/index.html

.. admonition:: info

   This tutorial was prepared for `PyCon 2021 <https://us.pycon.org/2021/schedule/presentation/23/>`_
   by `Mariatta <https://x.com/mariatta>`_.

Tutorial Description
=========================

The success of Python and open source libraries is not separable from the
availability of good documentation. Reading documentation is one of the first
things a user of the open source library has to do.

In the Python open source community, documentation is often written using
reStructuredText markup language, and built with Sphinx. The official Python
documentation and Python Enhancements Proposals (PEPs) are all written using
reStructuredText. Being able to write documentation using reStructuredText
becomes a necessary skill for any aspiring Python open source contributors
and maintainers.

Yet, reStructuredText itself can be seen as a barrier into contributing to open
source, since it is not as straightforward as Markdown. Compared to Markdown,
reStructuredText is not as widely adopted outside of the Python community.

Don't let this discourage you! Let's break down this barrier! reStructuredText
is not as complicated as you might think. You can learn it!

In this tutorial, we'll go through various useful features of reStructuredText.
You will learn how to create and build a documentation project using Sphinx.
Not only will you learn a new skill, you can also confidently start contributing
to open source projects by helping to improve their documentation.
