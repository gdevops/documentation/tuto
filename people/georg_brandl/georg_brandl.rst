.. index::
   pair: Georg ; Brandl

.. _georg_brandl_sphinx:

======================================
**Georg Brandl** (The sphinx creator)
======================================

- http://www.pocoo.org/team/
- https://x.com/birkenfeld


Sphinx
======

- http://sphinx-doc.org/


Welcome

What users say::

    “Cheers for a great tool that actually makes programmers want to write documentation!”



Sphinx is a tool that makes it easy to create intelligent and beautiful documentation,
written by Georg Brandl and licensed under the BSD license.

Old Presentation
===================

.. figure:: georg-brandl.png
   :align: center


Georg Brandl is a Python core developer since 2005, and **cares for its
documentation at docs.python.org**.

He is blogging on pythonic.pocoo.org (does not exist anymore)

His IRC nickname is birkenfeld, and you can contact him via email at georg@python.org.

Follow Georg on twitter: @birkenfeld (not used since 2012-04-24)



Georg Brandl and Brett Cannon to Receive PSF Community Awards (Thursday, August 07, 2008)
=========================================================================================

- http://pyfound.blogspot.fr/2008/08/georg-brandl-and-brett-cannon-to.html


At the July Board meeting of the PSF Board of Directors, PSF Community Awards
were awarded to Georg Brandl and Brett Cannon.

Georg has been an enthusiastic contributor to the core for several years, and a
**while ago stunned the Python development world by building the Sphinx
documentation system as an alternative to the LaTeX-based system we had been
using previously, and converting the Python documentation to use it**.

Brett has also been an active core developer for many years, but was nominated
for his infrastructure work in migrating the Python bug-tracking system off of
SourceForge to our own Roundup instance, and for his efforts keeping the Python
developer introduction updated.

Georg and Brett richly deserve recognition for their contributions.

Congratulations to Brett and Georg, and thanks for all your hard work!
