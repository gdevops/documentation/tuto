
.. _pdoc_def:

=======================
pdoc definition
=======================




https://github.com/pdoc3/pdoc
================================

Auto-generate API documentation for Python projects.


README.md pdoc
=================

.. seealso::

   - https://github.com/pdoc3/pdoc/blob/master/README.md

|Build Status| |Code Coverage| |pdoc3 on PyPI|

Auto-generate API documentation for Python projects.

`**Project website** <https://pdoc3.github.io/pdoc/>`__

`Documentation <https://pdoc3.github.io/pdoc/doc/pdoc/>`__

Installation
------------

::

    $ pip install pdoc3

Usage
-----

Pdoc will accept a Python module file, package directory or an import
path.

::

    $ pdoc your_project

See ``pdoc --help`` for more command-line switches and the
`documentation <https://pdoc3.github.io/pdoc/doc/pdoc/>`__ for more
usage examples.

Features
--------

-  Simple usage. Generate sensible API (+ prose) documentation without
   any special configuration.
-  Support for common docstrings formats (Markdown, numpydoc,
   Google-style docstrings) and some reST directives.
-  pdoc respects ``__all__`` when present.
-  Inheritance used as applicable for inferring docstrings for class
   members.
-  Support for documenting module, class, and instance variables by
   traversing ASTs.
-  Automatic cross-linking of referenced identifiers in HTML.
-  Overriding docstrings with special module-level ``__pdoc__``
   dictionary.
-  Built-in development web server for near instant preview of rendered
   docstrings.

The above features are explained in more detail in pdoc's
`documentation <https://pdoc3.github.io/pdoc/doc/pdoc/>`__ (which was
generated with pdoc).

.. |image0| image:: https://i.imgur.com/kQOtbBk.png
   :target: https://pdoc3.github.io/pdoc/
.. |Build Status| image:: https://img.shields.io/travis/pdoc3/pdoc.svg?style=for-the-badge
   :target: https://travis-ci.org/pdoc3/pdoc
.. |Code Coverage| image:: https://img.shields.io/codecov/c/gh/pdoc3/pdoc.svg?style=for-the-badge
   :target: https://codecov.io/gh/pdoc3/pdoc
.. |pdoc3 on PyPI| image:: https://img.shields.io/pypi/v/pdoc3.svg?style=for-the-badge
   :target: https://pypi.org/project/pdoc3


pdoc cli
=========

.. seealso::

   - https://pdoc3.github.io/pdoc/doc/pdoc/#command-line-interface

pdoc includes a feature-rich "binary" program for producing HTML and plain text
documentation of your modules.

To produce HTML documentation of your whole package in subdirectory 'build' of
the current directory, using the default HTML template, run:
