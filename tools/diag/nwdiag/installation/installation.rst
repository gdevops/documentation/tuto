

.. index::
   pair: nwdiag ; installation

.. _nwdiag_install:

=======================================
nwdiag installation
=======================================

.. seealso::

   - http://blockdiag.com/en/nwdiag/introduction.html#setup


::

    pip install nwdiag

::

	Collecting nwdiag
	  Downloading https://files.pythonhosted.org/packages/8e/06/42e672cc4b0efddd40f0c0de412dda7c29f8a971afb54b3d77579c28aa29/nwdiag-1.0.4-py2.py3-none-any.whl (7.7MB)
		 |████████████████████████████████| 7.7MB 1.5MB/s
	Requirement already satisfied: setuptools in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from nwdiag) (40.8.0)
	Requirement already satisfied: blockdiag>=1.5.0 in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from nwdiag) (1.5.4)
	Requirement already satisfied: Pillow in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from blockdiag>=1.5.0->nwdiag) (6.0.0)
	Requirement already satisfied: funcparserlib in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from blockdiag>=1.5.0->nwdiag) (0.3.6)
	Requirement already satisfied: webcolors in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from blockdiag>=1.5.0->nwdiag) (1.8.1)
	Installing collected packages: nwdiag
	Successfully installed nwdiag-1.0.4
