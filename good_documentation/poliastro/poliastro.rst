
.. index::
   pair: Poliastro ; Good documentation

.. _poliastro:

====================================================================
poliastro - Astrodynamics in Python
====================================================================


- https://docs.poliastro.space/en/stable/


1 All its documentation is written in Markdown using MyST (https://myst-parser.readthedocs.io) 🤯)
====================================================================================================

- https://x.com/readthedocs/status/1554462217067700225?s=20&t=iHx2y_VIiom2rE4AlQ1kNw
- https://myst-parser.readthedocs.io/en/latest/

2. It has plenty of amazing math formulas nicely rendered using nbsphinx)
============================================================================

- https://x.com/readthedocs/status/1554462227301863424?s=20&t=iHx2y_VIiom2rE4AlQ1kNw
- https://nbsphinx.readthedocs.io/en/0.8.9/
- https://www.mathjax.org/


3. Gallery full of examples using “poliastro” itself created with “nbsphinx”)
===============================================================================

- https://x.com/readthedocs/status/1554462232150409217?s=20&t=iHx2y_VIiom2rE4AlQ1kNw
- https://docs.poliastro.space/en/stable/gallery.html


4. INTERACTIVE 3D PLOTS WITH PLOTLY
=======================================

- https://x.com/readthedocs/status/1554462248768339969?s=20&t=iHx2y_VIiom2rE4AlQ1kNw
- https://docs.poliastro.space/en/stable/examples/Plotting%20in%203D.html
- https://plotly.com/python/3d-charts/


5. Nice 404 Not Found pages using “sphinx-notfound-page”
==========================================================

- https://sphinx-notfound-page.readthedocs.io/en/latest/


6. Massive usage of tooltips via “sphinx-hoverxref” (https://sphinx-hoverxref.readthedocs.io) to not lose context while you are reading
==========================================================================================================================================

- https://x.com/readthedocs/status/1554462268519235584?s=20&t=iHx2y_VIiom2rE4AlQ1kNw
- https://sphinx-hoverxref.readthedocs.io

7. Pretty nice split of pages following the concepts behind Diátaxis (https://diataxis.fr) to find out immediately what you are looking for! Split in these sections: Tutorials, How-To guides, Reference, Background, Links ✍️
==================================================================================================================================================================================================================================

- https://x.com/readthedocs/status/1554462273825013760?s=20&t=S0pud1slvTPyVrEOVPPBZQ
- https://diataxis.fr/


