
.. index::
   pair: Documentation ; swagger-ui
   ! swagger-ui

.. _swagger_ui:

=======================
swagger-ui
=======================

.. seealso::

   - https://github.com/swagger-api
   - https://github.com/swagger-api/swagger-ui
   - https://x.com/SwaggerApi

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
