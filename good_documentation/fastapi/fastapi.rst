
.. index::
   pair: Sebastián Ramírez ; Good documentation
   pair: Sebastián ; Ramírez
   pair: fastapi ; Good documentation

.. _fastapi_ramirez:

=====================================
**Fastapi** (from Sebastián Ramírez)
=====================================

.. seealso::

   - https://fastapi.tiangolo.com/
   - https://fastapi.tiangolo.com/contributing/
   - https://github.com/tiangolo/fastapi/tree/master/docs

