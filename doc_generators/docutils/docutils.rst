
.. index::
   pair: Documentation ; docutils
   ! docutils

.. _docutils:

============================================================================================
**docutils** (open-source text processing system for processing plaintext documentation )
============================================================================================

- https://docutils.sourceforge.io/
- https://sourceforge.net/projects/docutils/
- https://docutils.sourceforge.io/docs/index.html
- http://docutils.sourceforge.net/docs/user/links.html
- https://sourceforge.net/p/docutils/mailman/docutils-develop/


Github
=======

- https://github.com/live-clones/docutils
- https://github.com/AA-Turner/docutils

Description
===========

Docutils is an open-source text processing system for processing plaintext
documentation into useful formats, such as HTML, LaTeX, man-pages, OpenDocument,
or XML.

It includes reStructuredText, the easy to read, easy to use, what-you-see-is-what-you-get
plaintext markup language.


Front-end dev tools
====================

.. toctree::
   :maxdepth: 3

   rst2html/rst2html

Versions
=========

.. toctree::
   :maxdepth: 3

   versions/versions
