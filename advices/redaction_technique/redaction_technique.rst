
====================
Rédaction technique
====================

.. seealso::

   - http://redaction-technique.org/index.html


Libérez vos informations de leurs silos
=========================================

Des solutions souples et fiables libèrent vos informations des silos
d’information cloisonnés où elles sont emprisonnées et sous-exploitées.

Oubliez MS Word ou FrameMaker pour passer de la maintenance de la
documentation à la gestion du cycle de vie des projets documentaires
modulaires !



Intégrer la documentation aux processus de développement
=========================================================

La documentation fait partie du logiciel.
Fournie avec le produit, elle doit :

- sortir en même temps,
- suivre les mêmes cycles de vie, et
- faire l’objet des mêmes processus de production et de contrôle qualité.


.. figure:: integration-doc-dev.png
   :align: center



Elle doit répondre idéalement aux critères suivants :

- pas de vendor lock-in (**indépendance du format** et de l’éditeur de contenu),
- chaînes de publication **libres et gratuites**,
- mise en page totalement automatisée.

Il y a quelques années encore, les seuls outils permettant de fournir
des livrables de qualité au format PDF ou HTML reposaient sur des
formats binaires et propriétaires qui s’intégraient mal aux systèmes
de gestion de versions des équipes de développement.

Résultat : réalisée à part, la documentation technique répondait
difficilement aux mêmes exigences de qualité et de délai de mise sur
le marché que les produits.

**DocBook**, puis **DITA XML** et reStructuredText_ ont changé la donne : ces
formats texte peuvent être modifiés avec tout type de programme, du
simple éditeur de texte à l''IDE graphique, et s’intègrent parfaitement
sous Subversion, Git ou tout autre système de gestion de versions.


.. _reStructuredText:  https://pvergain.gitlab.io/pvbookmarks/documentation/doc_generators/sphinx/rest_sphinx/rest_sphinx.html#rest-sphinx

