

.. index::
   pair: pygments ; formatters

.. _pygemnts_formatters:

=======================
pygments formatters
=======================

.. seealso::

   - http://pygments.org/docs/formatters/
