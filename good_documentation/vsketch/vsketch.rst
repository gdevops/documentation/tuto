
.. index::
   pair: Good documentation ; vsketch

.. _vsketch:

============================
**vsketch** (for the API)
============================

- https://github.com/abey79/vsketch
- :ref:`sphinx_antoine_beyeler_autoapi`
