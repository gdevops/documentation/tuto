

.. index::
   pair: seqdiag ; Documentation
   ! seqdiag

.. _seqdiag:

=======================================
seqdiag
=======================================

.. seealso::

   - http://blockdiag.com/en/seqdiag/index.html
   - https://github.com/blockdiag/seqdiag

.. toctree::
   :maxdepth: 3

   installation/installation
