
.. index::
   ! Good documentation

.. _good_documentation:

==========================
Good docs
==========================

- :ref:`diataxis_2021_04_27`
- http://brikis98.blogspot.de/2014/05/you-are-what-you-document.html
- https://trafilatura.readthedocs.io/en/latest/downloads.html

.. toctree::
   :maxdepth: 3

   sphinx_intro_tutorial/sphinx_intro_tutorial
   geopandas/geopandas
   cakephp/cakephp
   cds_toolbox/cds_toolbox
   diataxis/diataxis
   fastapi/fastapi
   mattermost/mattermost
   poliastro/poliastro.rst
   pretalx/pretalx
   ray/ray
   sfepy/sfepy
   sphinx_material/sphinx_material
   vsketch/vsketch
