.. index::
   ! Sphinx
   pair: Documentation ; Sphinx

.. _tuto_sphinx:

===================================
**Sphinx** documentation builder
===================================


.. grid:: 1 2 3 3


   .. grid-item-card:: sphinx-copybutton
      :img-top: sphinx.png
      :link: tuto_sphinx:tuto_sphinx
      :link-type: ref
      :padding: 1

      Tuto sphinx


.. card:: Tuto sphinx
    :link: tuto_sphinx:tuto_sphinx
    :link-type: ref

    Tuto sphinx.
