


.. index::
   pair: Howto; Putting doc on Read the docs

.. _howto_read_the_docs:

===================================================
How to put sphinx documentation on Read the docs
===================================================


.. seealso::

   - https://gitlab.com/help/user/project/integrations/webhooks





Publicity
===========

.. figure:: publicite.png
   :align: center


Examples
=========

.. toctree::
   :maxdepth: 3

   exemple1/exemple1
   exemple2/exemple2
