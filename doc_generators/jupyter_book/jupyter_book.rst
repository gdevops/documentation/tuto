
.. index::
   pair: Documentation ; jupyter-book
   ! Jupyter-book

.. _jupyer_book:

============================================================================================
**jupyter-book** (Build interactive, publication-quality documents from Jupyter Notebooks )
============================================================================================

.. seealso::

   - https://github.com/executablebooks/jupyter-book
   - https://github.com/executablebooks/meta
   - https://github.com/executablebooks/jupyter-book/graphs/contributors
   - https://jupyterbook.org/intro.html
   - https://github.com/choldgraf
   - https://github.com/chrisjsewell
   - https://github.com/executablebooks/jupyter-book/tree/master/docs

.. toctree::
   :maxdepth: 3


   versions/versions

