.. index::
   pair: Diátaxis ; Framework
   ! Diátaxis

.. _diataxis_2021_04_27:

========================================================================================================
**Diátaxis Framework A systematic framework for technical documentation authoring** by Daniele Procida
========================================================================================================

.. seealso::

   - https://x.com/evildmp
   - https://github.com/evildmp
   - https://github.com/evildmp/diataxis-documentation-framework
   - https://diataxis.fr/
   - https://diataxis.fr/adoption/
   - https://diataxis.fr/how-to-use-diataxis/


..  rubric:: A systematic framework for technical documentation authoring.

============

.. rst-class:: quote

  The Grand Unified Theory of Documentation

.. rst-class:: attribution

  \- David Laing


=============




News
=====

.. seealso::

   - https://x.com/Arawa_fr/status/1382945602804219904?s=20
   - https://x.com/ericholscher/status/1386366518091341840?s=20
   - https://x.com/simonw/status/1386370167395942401?s=20
   - https://x.com/ben_nuttall/status/1382276372631064577?s=20


How to use Diátaxis
======================

- https://diataxis.fr/how-to-use-diataxis/

.. figure:: always-complete.jpg
   :align: center


Diátaxis Framework
=====================

A systematic framework for technical documentation authoring.

The Diátaxis framework aims to solve the problem of structure in technical
documentation.

It adopts a systematic approach to understanding the needs of documentation
users in their cycle of interaction with a product.


.. figure:: diataxis.png
   :align: center

   https://diataxis.fr/


Four modes of documentation
================================

Diátaxis
-----------------

From the Ancient Greek δῐᾰ́τᾰξῐς - dia, “across”, and taxis, “arrangement”.

The framework identifies four modes of documentation

- tutorials,
- how-to guides,
- technical reference
- and explanation.

Each of these modes (or types) answers to a different user need, fulfils
a different purpose and requires a different approach to its creation.

Technical documentation should be structured explicitly around these
four types, and should keep them all separate and distinct from each other.

In other words, what we call documentation is fundamentally not one thing,
but four.
Understanding the implications of this, and how those four different
things work, can help improve most documentation.

Diátaxis promises to make documentation and projects better, and the
teams that work with them more successful.


Light-weight and easily adopted
==================================

The framework is light-weight, easy to understand and straightforward
to apply. It doesn’t impose implementation constraints.

Complete and comprehensive

Diátaxis provides a comprehensive and nearly universally-applicable scheme,
that has been proven in practice across a wide variety of fields and
applications.

These include large and small, and open and proprietary documentation projects.


