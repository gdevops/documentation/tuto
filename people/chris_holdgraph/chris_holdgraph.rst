.. index::
   pair: Chris ; Holdgraph
   ! Chris Holdgraph

.. _chris_holdgraph:

==============================================
**Chris Holdgraph** sphinx, executablebook
==============================================

- https://hachyderm.io/@choldgraf
- https://chrisholdgraf.com/
- https://x.com/choldgraf
- https://github.com/choldgraf
- https://libraries.io/github/choldgraf
- http://predictablynoisy.com/
- https://github.com/choldgraf/sphinx-blog
- https://github.com/executablebooks/jupyter-book


Bio
=====

I work with Project Jupyter and other open communities to build tools
and solve problems for scientific research and education.


2020
======

Decided to get wacky and re-write my personal website/blog using Sphinx
--------------------------------------------------------------------------

- https://x.com/choldgraf/status/1309941944118771712?s=20
- https://predictablynoisy.com/
- https://github.com/choldgraf/sphinx-blog


Decided to get wacky and re-write `my personal website/blog <https://predictablynoisy.com/>`_ using Sphinx.

I am pretty happy with the result! Amazing how much this ecosystem has
improved in the last year

announcing-the-new-jupyter-book-cbf7aa8bc72e
------------------------------------------------


- https://blog.jupyter.org/announcing-the-new-jupyter-book-cbf7aa8bc72e


Jupyter Book is an open source project for building beautiful,
publication-quality books, websites, and documents from source material
that contains computational content.

With this post, we’re happy to announce that Jupyter Book has been re-written
from the ground up, making it easier to install, faster to use, and able
to create more complex publishing content in your books.

It is now supported by the Executable Book Project, an open community
that builds open source tools for interactive and executable documents
in the Jupyter ecosystem and beyond


Sphinx blog
=============

- https://github.com/choldgraf/sphinx-blog
- https://github.com/choldgraf/sphinx-blog/blob/f94a24cf5355ac5cc963a58f640bb57023b637bf/conf.py
