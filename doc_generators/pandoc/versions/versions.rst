
.. index::
   pair: Versions ; Pandoc

.. _pandoc_versions:

=======================
Pandoc Versions
=======================

.. seealso::

   - https://github.com/jgm/pandoc/releases

.. toctree::
   :maxdepth: 3

   2.9.2/2.9.2
