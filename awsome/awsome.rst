
.. index::
   pair: Documentation; Awsome

.. _awsome_doc:

==============================================
**awsome**
==============================================

- https://staticsitegenerators.net/
- https://jamstack.org/generators/
- http://blog.smartbear.com/software-quality/bid/256072/13-reasons-your-open-source-docs-make-people-want-to-scream


readthedocs-examples/awesome-read-the-docs
=============================================

- https://github.com/readthedocs-examples/awesome-read-the-docs


https://awesomesphinx.useblocks.com/
=======================================

- https://awesomesphinx.useblocks.com/

https://github.com/yoloseem/awesome-sphinxdoc
==================================================

- https://github.com/yoloseem/awesome-sphinxdoc
