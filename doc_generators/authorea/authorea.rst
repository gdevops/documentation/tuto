
.. index::
   pair: Documentation ; Authorea
   ! Authorea

.. _authorea:

=======================
Authorea
=======================


.. seealso::

   - https://www.authorea.com/




Introduction
=============

Authorea is the collaborative platform for research.

Write and manage your technical documents in one place.
