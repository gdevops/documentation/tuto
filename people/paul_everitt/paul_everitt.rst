.. index::
   pair: People; Paul Everitt


.. _paul_everitt:

=========================================================================
**Paul Everitt** (https://x.com/paulweveritt, sphinx)
=========================================================================


- https://x.com/paulweveritt
- https://github.com/pauleveritt
- https://dev.to/pauleveritt
- https://x.com/paulweveritt/status/1383083106689818640?s=20


.. figure:: paul_everitt.jpg
   :align: center


Sphinx projet
=================

.. figure:: projet.png
   :align: center


jetbrains_guide
==================

- https://www.jetbrains.com/pycharm/guide/tutorials/sphinx_sites/
- https://www.jetbrains.com/pycharm/guide/tutorials/sphinx_sites/basic_formatting/

- https://github.com/JetBrains/jetbrains_guide  ?
- https://github.com/JetBrains/jetbrains_guide/tree/main/sites/pycharm-guide/demos/tutorials/sphinx_sites ?
