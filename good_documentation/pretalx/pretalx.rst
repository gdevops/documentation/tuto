
.. index::
   pair: pretalx ; documentation
   pair: pretalx ; Good documentation


.. _pretalx_documentation:

=========================================================================
pretalx (Conference planning tool: CfP, scheduling, speaker management)
=========================================================================

.. seealso::

   - https://pretalx.com/p/about/
   - https://github.com/pretalx/pretalx/tree/master/doc
   - https://pretalx.readthedocs.io/en/latest/contents.html
   - https://github.com/pretalx/pretalx/blob/master/doc/conf.py





conf.py
==========

.. seealso::

   - https://github.com/pretalx/pretalx/blob/master/doc/conf.py


Contents
==========

.. seealso::

   - https://github.com/pretalx/pretalx/tree/master/doc
   - https://pretalx.readthedocs.io/en/latest/contents.html

administrator
===============

.. seealso::

   - https://github.com/pretalx/pretalx/tree/master/doc/administrator


Developer
=========

.. seealso::

   - https://github.com/pretalx/pretalx/tree/master/doc/developer


API
=====

.. seealso::

   - https://github.com/pretalx/pretalx/tree/master/doc/api



Maintainer
============

.. seealso::

   - https://github.com/pretalx/pretalx/tree/master/doc/maintainer


Commit messages
================

.. seealso::

   - https://pretalx.readthedocs.io/en/latest/developer/contributing.html#commit-messages
   - https://raw.githubusercontent.com/pretalx/pretalx/master/doc/developer/contributing.rst


Please wrap all lines of your commit messages at 72 characters at most – bonus
points if your first line is no longer than 50 characters. If your commit
message is longer than one line, the first line should be the subject, the
second line should be empty, and the remainder should be text.

If you want to address or close issues, please do so in the commit message
body. ``Closes #123`` or ``Refs #123`` will close the issue or show the
reference in the issue log.

To make our unpaid, for-fun development process less dreary and more fun, we
tend to include emoji in our commit messages. You don't have to do so, but if
you do, please note that these are the meanings we ascribe to them:

+----+--------------------+
| ✨ | Feature            |
+----+--------------------+
| 🐛 | Bug                |
+----+--------------------+
| 🎀 | UI improvement     |
+----+--------------------+
| 📚 | Documentation      |
+----+--------------------+
| 🐎 | Performance        |
+----+--------------------+
| 🎨 | Code style         |
+----+--------------------+
| 🔥 | Code removal       |
+----+--------------------+
| 🔨 | Refactoring        |
+----+--------------------+
| ☔ | Tests              |
+----+--------------------+
| 🔒 | Security issue     |
+----+--------------------+
| ⬆  | Dependency upgrade |
+----+--------------------+
| 🚨 | Fix CI build       |
+----+--------------------+
| 🧹 | Housekeeping       |
+----+--------------------+
| 📦 | Packaging          |
+----+--------------------+
| 🚀 | Release            |
+----+--------------------+
| 🗺  | Translations       |
+----+--------------------+
