
.. index::
   pair: Documentation ; executablebooks
   ! executablebooks

.. _executablebooks:

=================================================================================================
**executablebooks** (Build interactive, publication-quality documents from Jupyter Notebooks )
=================================================================================================

.. seealso::

   - https://github.com/executablebooks
   - https://gdevops.gitlab.io/tuto_executablebooks
   - https://executablebooks.org/en/latest/
