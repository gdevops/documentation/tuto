.. index::
   pair: pygments ; issues

.. _pygments_issues:

=======================
pygments issues
=======================

.. seealso::

   - https://github.com/pygments/pygments/issues
