

.. index::
   pair: seqdiag ; installation

.. _seqdiag_install:

=======================================
seqdiag installation
=======================================

.. seealso::

   - http://blockdiag.com/en/seqdiag/introduction.html#setuptools


::

    pip install seqdiag

::

	Collecting seqdiag
	  Downloading https://files.pythonhosted.org/packages/0d/45/c4afb6fb9bb7c16e28d4bedaab15195792fd49448edde0f202e850f15764/seqdiag-0.9.6-py2.py3-none-any.whl (2.6MB)
		 |████████████████████████████████| 2.6MB 1.3MB/s
	Requirement already satisfied: blockdiag>=1.5.0 in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from seqdiag) (1.5.4)
	Requirement already satisfied: webcolors in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from blockdiag>=1.5.0->seqdiag) (1.8.1)
	Requirement already satisfied: funcparserlib in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from blockdiag>=1.5.0->seqdiag) (0.3.6)
	Requirement already satisfied: Pillow in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from blockdiag>=1.5.0->seqdiag) (6.0.0)
	Requirement already satisfied: setuptools in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from blockdiag>=1.5.0->seqdiag) (40.8.0)
	Installing collected packages: seqdiag
	Successfully installed seqdiag-0.9.6
