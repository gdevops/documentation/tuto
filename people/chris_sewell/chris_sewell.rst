.. index::
   pair: Chris ; Sewell
   ! Chris Sewell

.. _chris_sewell:

======================================
**Chris Sewell** (MyST-Parser)
======================================

- https://x.com/chrisj_sewell
- https://x.com/chrisj_sewell/with_replies
- https://x.com/chrisj_sewell/likes
- https://github.com/executablebooks/MyST-Parser
- https://github.com/chrisjsewell
- https://github.com/executablebooks/markdown-it-py
- https://github.com/executablebooks/MyST-Parser
- https://github.com/aiidateam/aiida-core



Bio
======

Open source developer and materials researcher.

Working towards open source, reproducible and shareable science solutions
grinning


MyST-Parser
=============

- https://github.com/executablebooks/MyST-Parser
- https://github.com/executablebooks/MyST-Parser/graphs/contributors


.. figure:: myst_contributors.png
   :align: center
