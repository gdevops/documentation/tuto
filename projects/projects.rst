.. index::
   pair: Documentation ; projects

.. _documentation_projects:

=======================
Projects
=======================

.. toctree::
   :maxdepth: 3


   c/index
   mozilla/mozilla
   python/index
