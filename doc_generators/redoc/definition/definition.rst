
.. _redoc_def:

=======================
redoc definition
=======================

.. seealso::

   - https://github.com/Rebilly/ReDoc




Github openAPI definition
===========================

.. seealso::

   - https://github.com/Rebilly/ReDoc/blob/master/README.md

OpenAPI/Swagger-generated API Reference Documentation.

.. include:: README.rst
