

.. _docutils_0_19_0:

============================================================================================
**docutils 0.19.0b (unpublished)**
============================================================================================

- https://docutils.sourceforge.io/HISTORY.html


- Drop support for Python 2.7, 3.5, and 3.6.
- Support parsing "Markdown" input with 3rd party parsers myst, pycmark,
  or recommonmark.
- The default values for the "pep-references", "rfc-base-url", and
  "python-home" configuration settings now use the "https:" scheme.
  The PEP-writer template's header is updated to fix links and resemble
  the header of official PEPs.
- Various bugfixes and improvements (see HISTORY).
