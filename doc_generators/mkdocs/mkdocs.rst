
.. index::
   pair: Documentation ; Mkdocs
   pair: Sebastián Ramírez ; Mkdocs
   ! Mkdocs

.. _mkdocs:

==============================
**Mkdocs (mkdocs-material)**
==============================

- http://www.mkdocs.org/
- https://squidfunk.github.io/mkdocs-material/
- https://docs.readthedocs.io/en/stable/intro/getting-started-with-mkdocs.html


Overview
=========

``MkDocs`` is a fast, simple and downright gorgeous static site generator that's
geared towards building project documentation.

Documentation source files are written in Markdown, and configured with a single
YAML configuration file.

The documentation site that we've just built only uses static files so you'll
be able to host it from pretty much anywhere.

GitHub project pages and Amazon S3 are good hosting options.

Upload the contents of the entire site directory to wherever you're hosting
your website from and you're done.


Themes
=======

- https://squidfunk.github.io/mkdocs-material/


News
======

.. toctree::
   :maxdepth: 3

   news/news

Nice docs
===========

- :ref:`fastapi_ramirez`


Used by
==========

formation-linux.microlinux.fr
------------------------------

- https://formation-linux.microlinux.fr/

pdm
-----

- https://pdm.fming.dev/

pipx
------

- https://pypa.github.io/pipx/

