.. index::
   pair: People; Juan Luis


.. _juan_luis:

=========================================================================
**Juan Luis** (https://x.com/juanluisback, sphinx, read the docs)
=========================================================================


- https://x.com/juanluisback
- https://x.com/juanluisback/status/1371527152718479361?s=20


.. figure:: juan_luis.jpg
   :align: center

Bio
=====

Developer Advocate @readthedocs with a focus on Science and Data projects.

**Open Knowledge + Radical Transparency**. Trying to make a positive impact.
he/him/his.


2021
====

- :ref:`unconference_2021_04_28`
