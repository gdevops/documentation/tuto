.. index::
   ! Pradyun Gedam

.. _pradyun_gedam:

======================================
**Pradyun Gedam** (sphinx furo)
======================================


.. seealso::

   - https://x.com/pradyunsg
   - https://github.com/pradyunsg
   - https://pradyunsg.me/




Pradyun Gedam
================

A tiny human. I work on "useless" software like pip, virtualenv, sphinx-doc,
TOML, and more


Author of:

- :ref:`furo_theme`
- :ref:`sphinx_inline_tabs`

Works on:

- https://github.com/sphinx-themes/sphinx-themes.org
