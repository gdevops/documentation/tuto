
.. index::
   pair: swagger-ui ; Versions

.. _swagger_ui_versions:

=======================
swagger-ui versions
=======================

.. seealso::

   - https://github.com/swagger-api/swagger-ui/releases

.. toctree::
   :maxdepth: 3

   3.22.1/3.22.1
