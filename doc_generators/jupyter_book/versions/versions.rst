
.. index::
   pair: Documentation ; jupyter-book
   ! Jupyter-book

.. _jupyer_book_versions:

============================================================================================
jupyter-book versions
============================================================================================

.. seealso::

   - https://github.com/executablebooks/jupyter-book
   - https://github.com/executablebooks/jupyter-book/graphs/contributors
   - https://jupyterbook.org/reference/_changelog.html

.. toctree::
   :maxdepth: 3


   0.8.1/0.8.1

