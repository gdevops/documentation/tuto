
.. _jinja_versions:

============================
**jinja versions**
============================

- https://jinja.palletsprojects.com/en/3.1.x/


.. toctree::
   :maxdepth: 3

   3.1.2/3.1.2
   3.0.0/3.0.0
