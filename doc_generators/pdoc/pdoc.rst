
.. index::
   pair: Documentation ; pdoc
   ! pdoc

.. _pdoc:

============================================================
pdoc (Auto-generate API documentation for Python projects)
============================================================

.. seealso::

   - https://github.com/pdoc3/pdoc

.. figure:: pdoc_logo.png
   :align: center
   :width: 300

.. toctree::
   :maxdepth: 3

   definition/definition
   examples/examples
   versions/versions
