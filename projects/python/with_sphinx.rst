

========================================
Documenting python projects with sphinx
========================================


.. seealso::

   - :ref:`documenting_with_sphinx`
   - http://sphinx-doc.org/latest
   - http://packages.python.org/an_example_pypi_project/sphinx.html
   - http://docs.python.org/dev/documenting/


sphinx
======

.. seealso::

   - http://sphinx-doc.org/latest

an example pypi project
=======================

.. seealso::

   - http://packages.python.org/an_example_pypi_project/sphinx.html



http://docs.python.org/dev/documenting
==============================================

.. seealso:: http://docs.python.org/dev/documenting/
