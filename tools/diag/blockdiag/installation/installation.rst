

.. _blockdiag_install:

=======================================
blockdiag installation
=======================================

.. seealso::

   - http://blockdiag.com/en/blockdiag/index.html


::

    pip install blockdiag

::

	Collecting blockdiag
	Downloading https://files.pythonhosted.org/packages/e6/37/a3a4d09c8cbe16b303ed75fd07381e5460b37a25fe247645f2251477887a/blockdiag-1.5.4-py2.py3-none-any.whl (2.7MB)
	|████████████████████████████████| 2.7MB 1.1MB/s
	Collecting Pillow (from blockdiag)
	Downloading https://files.pythonhosted.org/packages/c1/e6/ce127fa0ac17775bc7887c432ffe945c49ae141f01b477b7cd5e63b16bb5/Pillow-6.0.0-cp37-cp37m-manylinux1_x86_64.whl (2.0MB)
	|████████████████████████████████| 2.0MB 1.6MB/s
	Collecting webcolors (from blockdiag)
	Downloading https://files.pythonhosted.org/packages/1d/44/c4902683be73beba20afd299705e11f0a753a01cc7f9d6a070841848605b/webcolors-1.8.1-py2.py3-none-any.whl
	Collecting funcparserlib (from blockdiag)
	Downloading https://files.pythonhosted.org/packages/cb/f7/b4a59c3ccf67c0082546eaeb454da1a6610e924d2e7a2a21f337ecae7b40/funcparserlib-0.3.6.tar.gz
	Requirement already satisfied: setuptools in ./.pyenv/versions/3.7.3/lib/python3.7/site-packages (from blockdiag) (40.8.0)
	Installing collected packages: Pillow, webcolors, funcparserlib, blockdiag
	Running setup.py install for funcparserlib ... done
	Successfully installed Pillow-6.0.0 blockdiag-1.5.4 funcparserlib-0.3.6 webcolors-1.8.1
