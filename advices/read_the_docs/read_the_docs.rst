

.. _write_the_docs_bis:

===============================
Read the docs/Write the docs
===============================

.. seealso::

   - https://docs.readthedocs.io/en/stable/
   - https://docs.readthedocs.io/en/stable/intro/getting-started-with-sphinx.html
   - https://x.com/readthedocs
   - https://x.com/writethedocs

