

.. _swagger_ui_def:

=======================
swagger-ui definition
=======================

.. seealso::

   - https://github.com/swagger-api/swagger-ui




github swagger_ui definition
==============================


Swagger UI is a collection of HTML, Javascript, and CSS assets that dynamically
generate beautiful documentation from a Swagger-compliant API.
