
.. _jinja_3_0_0:

==============================
**jinja 3.0.0 (2021-05-12)**
==============================

- https://palletsprojects.com/blog/flask-2-0-released/
- https://github.com/pallets/jinja/releases/tag/3.0.0
- https://jinja.palletsprojects.com/en/3.1.x/changes/#version-3-0-0
- https://x.com/PalletsTeam/status/1392266507296514048?s=20&t=7DzQPgaeLOGkAE-Lg9r80A

.. figure:: images/announce.png
   :align: center

   https://github.com/pallets/jinja/releases/tag/3.0.0


Announce
==========

New major versions of all the core Pallets libraries, including Jinja 3.0,
have been released! tada

- Read the announcement on our blog: https://palletsprojects.com/blog/flask-2-0-released/
- Read the full list of changes: https://jinja.palletsprojects.com/changes/#version-3-0-0
- Retweet the announcement on Twitter: https://x.com/PalletsTeam/status/1392266507296514048
- Follow our blog, Twitter, or GitHub to see future announcements.

**This represents a significant amount of work**, and there are quite a few
changes.

Be sure to carefully read the changelog, and use tools such as pip-compile
and Dependabot to pin your dependencies and control your updates.

