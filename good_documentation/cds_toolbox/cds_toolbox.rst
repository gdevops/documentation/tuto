
.. index::
   pair: CDS toolbox ; Good documentation

.. _cds_toolbox_documentation:

====================================================================
Sphinx CDS (**Climate Data Store**) toolbox documentation
====================================================================


- https://cds.climate.copernicus.eu/toolbox/doc/index.html
- https://github.com/ecmwf/
- https://github.com/ecmwf/cdsapi
- https://x.com/v_mavel/status/1265595493880672256?s=20
- https://x.com/pvergain/status/1265941948910776320?s=20
- https://x.com/SabrinaSzeto
- https://x.com/JuliaWagemann
- https://x.com/bopensolutions
- https://x.com/ejrquartz
- https://x.com/CopernicusECMWF


.. figure:: julia_wagemann.png
   :align: center

   https://x.com/JuliaWagemann/status/1265587230866841602?s=20
