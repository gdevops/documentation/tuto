
.. index::
   pair: Versions ; Hugo

.. _hugo_versions:

=======================
Hugo versions
=======================

.. seealso::

   - https://github.com/gohugoio/hugo/releases

.. toctree::
   :maxdepth: 3

   0.55.2/0.55.2
