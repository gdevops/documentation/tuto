
.. index::
   pair: Mattermost ; Good documentation

.. _mattermost_doc:

============
Mattermost
============

.. seealso::

   - https://docs.mattermost.com/
   - https://github.com/mattermost/docs
