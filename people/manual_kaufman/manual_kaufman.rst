.. index::
   pair: People; Manuel Kaufman

.. _manuel_kaufman:

=========================================================================================================
**Manuel Kaufmann** (humitos, https://x.com/reydelhumo sphinx, read the docs, sphinx-material)
=========================================================================================================


- https://github.com/humitos
- https://x.com/reydelhumo


.. figure:: manual_kaufman.jpeg
   :align: center

Bio
=====

Working at @readthedocs and trying to save the world.


Projects
===========

sphinx-hoverxref
------------------

- https://github.com/readthedocs/sphinx-hoverxref

sphinx-extensions
-------------------

- https://sphinx-extensions.readthedocs.io/en/latest/ (with sphinx-material)

sphinx-hoverxref
-------------------

- https://sphinx-hoverxref.readthedocs.io/en/latest/

