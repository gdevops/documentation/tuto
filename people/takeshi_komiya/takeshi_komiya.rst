.. index::
   pair: Takeshi ; Komiya
   ! Takeshi Komiya

.. _takeshi_komiya:

======================================
**Takeshi Komiya** (sphinx core)
======================================

.. seealso::

   - https://github.com/sponsors/tk0miya
   - https://github.com/tk0miya
   - https://x.com/tk0miya




Bio
======

Hello! I'm @tk0miya, **a maintainer of Sphinx**, an Open Source
Software which build beautiful documentation in **several formats**.

It has been used by many open source softwares (ex. Python, Linux Kernel and so on).

I have been contributing to the its ecosystem since 2014 and joined to Sphinx
project since 2015.

In last 3years, I've contributed a large amount of changes into Sphinx repos
(about 60+%).

If you have a fun for Sphinx, please consider buying me a cup of tea in a
while coffee :-)
