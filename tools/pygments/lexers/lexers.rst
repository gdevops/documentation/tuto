

.. index::
   pair: pygments ; lexers

.. _pygemnts_lexers:

=======================
pygments lexers
=======================

.. seealso::

   - http://pygments.org/docs/lexers/
   - http://pygments.org/docs/lexerdevelopment/
