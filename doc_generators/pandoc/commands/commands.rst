
.. index::
   pair: Commands ; Pandoc

.. _pandoc_commands:

=======================
Pandoc commands
=======================





.. _md_to_rst:

README.md => README.rst (markdown to rst)
===========================================


::

    pandoc README.md -o README.rst
