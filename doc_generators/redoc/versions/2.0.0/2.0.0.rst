
.. index::
   pair: redoc ; 2.0.0 (NOT RELEASED)

.. _redoc_2_0_0:

============================
redoc 2.0.0 (NOT RELEASED)
============================

.. seealso::

   - https://github.com/Rebilly/ReDoc/releases/tag/v2.0.0-rc.4
