
.. index::
   pair: Versions ; docutils

.. _docutils_versions:

============================================================================================
**docutils** versions
============================================================================================

- https://docutils.sourceforge.io/HISTORY.html
- https://sourceforge.net/p/docutils/mailman/docutils-develop/

.. toctree::
   :maxdepth: 3

   0.19/0.19
   0.18.1/0.18.1
   0.18/0.18
   0.17/0.17
   0.12/0.12
   0.11/0.11
