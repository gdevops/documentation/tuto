.. index::
   pair: pygments ; 2.8.0 (2021-02-14)

.. _pygments_2_8_0:

=================================
pygments 2.8.0 (2021-02-14)
=================================


- https://github.com/pygments/pygments/releases/tag/2.9.0
- https://pygments.org/docs/changelog/
- https://github.com/pygments/pygments/blob/master/CHANGES


Version 2.8.0
================

- Added lexers:

    * AMDGPU (#1626)
    * CDDL (#1379, #1239)
    * Futhark (#1691)
    * Graphviz/DOT (#1657, #731)
