

.. index::
   ! interactive.blockdiag

.. _interactive_blockdiag:

=======================================
interactive.blockdiag
=======================================

.. seealso::

   - https://bitbucket.org/blockdiag/blockdiag_interactive_shell
   - http://interactive.blockdiag.com/
