.. index::
   pair: Gitlab ; Sphinx
   pair: Continuous Deployment; Gitlab
   pair: pipenv; requirements.txt

.. _sphinx_gitlab:

==========================================
Sphinx documentation on **gitlab pages**
==========================================

.. seealso::

   - https://gitlab.com/gdevops/tuto_documentation
   - https://gdevops.gitlab.io/tuto_documentation/index.html
   - https://docs.gitlab.com/ce/user/project/pages/#explore-gitlab-pages



Continuous deployment of the documentation
===========================================

.. seealso::

   - https://gitlab.com/gdevops/tuto_documentation/-/blob/master/.gitlab-ci.yml
   - https://gitlab.com/gdevops/tuto_documentation/-/blob/master/requirements.txt

To produce your sphinx documentation, simply put this *.gitlab-ci.yml* file
at the root of your documentation.

.. literalinclude:: ../../.gitlab-ci.yml
   :linenos:
   :language: yaml
   

If you use pipenv for your sphinx documentation, produce the requirements.txt
file with this command::

    pipenv lock -r > requirements.txt


::

	total 200
	 4 drwxr-xr-x 10 pvergain doc  4096 mai   21 12:53 ./
	 4 drwxrwxr-x 14 pvergain doc  4096 mai   20 19:41 ../
	 4 drwxr-xr-x  4 pvergain doc  4096 mai   21 11:31 _build/
	68 -rw-r--r--  1 pvergain doc 66507 mai   16 22:28 charac-more.png
	 4 drwxr-xr-x  7 pvergain doc  4096 mai   16 22:28 cli/
	 8 -rw-r--r--  1 pvergain doc  5786 mai   21 11:23 conf.py
	 4 drwxr-xr-x  4 pvergain doc  4096 mai   16 22:28 devel/
	 4 drwxr-xr-x 12 pvergain doc  4096 mai   21 11:29 documentation/
	 4 drwxr-xr-x  8 pvergain doc  4096 mai   21 14:31 .git/
	 4 -rw-r--r--  1 pvergain doc   140 mai   20 18:40 .gitignore
	 4 -rw-r--r--  1 pvergain doc   212 mai   16 22:28 .gitlab-ci.yml
	 4 -rw-r--r--  1 pvergain doc   920 mai   21 12:53 index.rst
	 4 -rw-r--r--  1 pvergain doc   651 mai   16 22:28 Makefile
	16 -rw-r--r--  1 pvergain doc 15811 mai   21 14:31 objects.inv
	 4 -rw-r--r--  1 pvergain doc   158 mai   16 22:28 Pipfile
	 8 -rw-r--r--  1 pvergain doc  6388 mai   21 11:12 Pipfile.lock
	 4 -rw-r--r--  1 pvergain doc   327 mai   21 12:26 README.md
	 4 -rw-r--r--  1 pvergain doc   344 mai   20 18:39 requirements.txt
	32 -rw-r--r--  1 pvergain doc 29330 mai   16 22:28 small-globe.png
	 4 drwxr-xr-x  2 pvergain doc  4096 mai   16 22:28 _static/
	 4 drwxr-xr-x  4 pvergain doc  4096 mai   16 22:28 _themes/
	 4 drwxr-xr-x  4 pvergain doc  4096 mai   16 22:29 .venv/
