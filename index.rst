.. Documentation tutorial documentation master file, created by
   sphinx-quickstart on Thu Mar 22 19:57:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. figure:: images/documentation.png
   :align: center
   :width: 300

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/documentation/tuto/rss.xml>`_

.. _tuto_doc:

==========================
Documentation tutorial
==========================

- http://blog.smartbear.com/software-quality/bid/256072/13-reasons-your-open-source-docs-make-people-want-to-scream
- http://brikis98.blogspot.de/2014/05/you-are-what-you-document.html
- http://redaction-technique.org/index.html
- https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/reflexpro/syntaxe_rest.html

Our favorite document generator is :ref:`sphinx <tuto_sphinx>` and we use the
:ref:`reStructuredText markup <rest_sphinx_doc>` and :ref:`sphinx-design <sphinx_design>`

..  epigraph::

    I'm not impressed if you're a good engineer. I'm impressed if you're a
    good teammate.

    https://x.com/RandallKanna/status/1387202807103574016?s=20


.. toctree::
   :maxdepth: 6
   :caption: Awsome Documentation news

   awsome/awsome

.. toctree::
   :maxdepth: 3
   :caption: Documentation

   people/people
   advices/advices
   good_documentation/good_documentation
   doc_generators/doc_generators
   hebergeurs/hebergeurs
   formats/formats
   projects/projects
   tools/tools

