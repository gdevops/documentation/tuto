
.. index::
   pair: Documentation; Generators

.. _documentation_generators:

==============================================
Document generators (sphinx, mkdocs, etc..)
==============================================

- https://github.com/yoloseem/awesome-sphinxdoc
- https://staticsitegenerators.net/
- https://jamstack.org/generators/
- http://blog.smartbear.com/software-quality/bid/256072/13-reasons-your-open-source-docs-make-people-want-to-scream


.. toctree::
   :maxdepth: 2

   sphinx/sphinx
   docutils/docutils
   executablebooks/executablebooks
   jupyter_book/jupyter_book
   bookdown/bookdown
   authorea/authorea
   doxygen/doxygen
   gatsby/gatsby
   hugo/hugo
   javadoc/javadoc
   jekyll/jekyll
   jsdoc/jsdoc
   mdbook/mdbook
   mkdocs/mkdocs
   pandoc/pandoc
   pdoc/pdoc
   redoc/redoc
   swagger_ui/swagger_ui

